import React from 'react'

const User = ({ user }) => {
  return (
    <div className="User">
      <h1>{ user.name }</h1>
      <p>{ user.username }</p>
      <p>{ user.email }</p>
    </div>
  )
}

export default User
