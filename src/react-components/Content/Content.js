import React from 'react'
import { Switch, Route } from 'react-router-dom'
import InvoiceTable from './Table'
import Profile from './Profile/Profile'
import Settings from './Settings/Settings'
import Charts from '../Charts/Charts'
import Signup from '../Account/Signup/Signup'
import Login from '../Account/Login/Login'
import ReloadServiceModal from '../Modals/ReloadServiceModal'

const Content = () => {
  return (
    <section className="c-layout-dashboard__main">
      <main>
        <article className="o-card u-box--shadow u-no-margins">
          <div className="o-card__inner">
            <div className="o-card__body">
              <Switch>
                <Route path={['/', '/bills']} exact={true}>
                  <InvoiceTable />
                </Route>
                <Route path="/profile">
                  <Profile />
                </Route>
                <Route path="/settings">
                  <Settings />
                </Route>
                <Route path="/charts">
                  <Charts />
                </Route>
                <Route path="/signup">
                  <Signup />
                </Route>
                <Route path="/login">
                  <Login />
                </Route>
              </Switch>
            </div>
          </div>
        </article>
      </main>
      <ReloadServiceModal />
    </section>
  )
}

export default Content
