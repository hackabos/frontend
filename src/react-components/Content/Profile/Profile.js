import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Icon, Button } from 'react-materialize'
import { sendForm } from '../../../redux/ducks/forms'
import PasswordInput from '../../Utils/PasswordInput'
import './Profile.scss'

const Profile = () => {
  const [passValidation, setPassValidation] = useState(true)
  const dispatch = useDispatch()
  const { profile } = useSelector(s => s.user)
  const [form, setForm] = useState({ ...profile })
  const handleChange = e => {
    setForm({ ...form, [e.target.id]: e.target.value })
  }
  const handleKeyUp = e => {
    setPassValidation(form.password === form['re-password'])
  }

  const handleSubmit = e => {
    e.preventDefault()

    if (!passValidation) return

    dispatch(sendForm('profileForm', form))

    const { password, 're-password': rePassword, ...rest } = form
    setForm(rest)
  }

  return (
    <div className="Profile c-row">
      <section className="o-card c-col-4">
        <div className="o-card__inner u-box--no-padding">
          <header className="o-card__header profile-image">
            <Icon>account_circle</Icon>
          </header>
          <main className="o-card__body profile-data">
            <h1>{profile.username}</h1>
          </main>
        </div>
      </section>

      <section className="o-card c-col-8">
        <div className="o-card__inner u-box--no-padding">
          <header className="o-card__header">
            <h1>ACCOUNT DATA</h1>
          </header>
          <form id="profile-form" className='c-form-float' autoComplete='off' onSubmit={handleSubmit}>
            <main className="o-card__body">
              <div className='c-form__input-group'>
                <input
                  id='username'
                  className='c-float-input'
                  type='text'
                  placeholder=' '
                  value={form.username || ''}
                  onChange={handleChange}
                  disabled
                  required
                />
                <label className='c-float-label' htmlFor='username'>EMAIL</label>
              </div>
              <div className='c-form__input-group'>
                <input
                  id='name'
                  className='c-float-input'
                  type='text'
                  placeholder=' '
                  value={form.name || ''}
                  onChange={handleChange}
                />
                <label className='c-float-label' htmlFor='name'>NAME</label>
              </div>
              <div className='c-form__input-group'>
                <input
                  id='surname'
                  className='c-float-input'
                  type='text'
                  placeholder=' '
                  value={form.surname || ''}
                  onChange={handleChange}
                />
                <label className='c-float-label' htmlFor='surname'>SURNAME</label>
              </div>
            </main>
            <header className="o-card__header">
              <h1>CHANGE PASSWORD</h1>
            </header>
            <main className="o-card__body">
              {/* <div className='c-form__input-group'>
                <input
                  id='password'
                  className='c-float-input'
                  type='password'
                  placeholder=' '
                  value={form.password || ''}
                  onChange={handleChange}
                  onKeyUp={handleKeyUp}
                />
                <label className='c-float-label' htmlFor='password'>PASSWORD</label>
                {!passValidation && <small>Passwords don`t match</small>}
              </div>
              <div className='c-form__input-group'>
                <input
                  id='re-password'
                  className='c-float-input'
                  type='password'
                  placeholder=' '
                  value={form['re-password'] || ''}
                  onChange={handleChange}
                  onKeyUp={handleKeyUp}
                />
                <label className='c-float-label' htmlFor='re-password'>RE-TYPE PASSWORD</label>
                {!passValidation && <small>Passwords don`t match</small>}
              </div> */}

              <PasswordInput
                value={form.password || ''}
                onChange={handleChange}
                onKeyUp={handleKeyUp}
                id='password'
                label="PASSWORD"
              >
                {!passValidation && <small>Passwords don`t match</small>}
              </PasswordInput>
              <PasswordInput
                value={form['re-password'] || ''}
                onChange={handleChange}
                onKeyUp={handleKeyUp}
                id='re-password'
                label="RE-TYPE PASSWORD"
              >
                {!passValidation && <small>Passwords don`t match</small>}
              </PasswordInput>
            </main>
          </form>
          <footer className="o-card__footer">
            <Button type="submit" form="profile-form">Save changes</Button>
          </footer>
        </div>
      </section>
    </div>
  )
}

export default Profile
