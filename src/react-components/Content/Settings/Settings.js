import React from 'react'
import UserServices from './Services/UserServices'

const Settings = () => {
  return (
    <div className="Settings">
      <section className="o-card">
        <div className="o-card__inner u-box--no-padding">
          <header className="o-card__header">
            <h1>MANAGE YOUR SERVICES</h1>
          </header>
          <main className="o-card__body">
            <UserServices />
          </main>
        </div>
      </section>
    </div>
  )
}

export default Settings
