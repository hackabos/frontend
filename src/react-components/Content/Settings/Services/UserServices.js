import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Button, Icon } from 'react-materialize'
import { getUserServicesAction } from '../../../../redux/ducks/services'
import AddUserService from './AddUserService'
import UserServiceItem from './UserServiceItem'
import './UserService.scss'

const UserServices = () => {
  const dispatch = useDispatch()
  const { userServices } = useSelector(s => s.services)
  const [isCreating, setCreating] = useState(false)
  const handleSave = () => setCreating(false)
  const handleClick = () => setCreating(true)

  useEffect(() => {
    dispatch(getUserServicesAction())
  }, [dispatch])

  return (
    <>
      <ul>
        {userServices && userServices.map(userService =>
          <li key={userService.id} data-id={userService.id}><UserServiceItem userService={userService} /></li>
        )}
      </ul>
      {isCreating ? <AddUserService onSave={handleSave} /> : <Button floating large icon={<Icon>add</Icon>} onClick={handleClick}>Add</Button>}
    </>
  )
}

export default UserServices
