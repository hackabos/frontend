import React, { useState, useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { Button, Icon } from 'react-materialize'
import SelectService from './SelectService'
import { saveUserServiceAction } from '../../../../redux/ducks/services'
import PasswordInput from '../../../Utils/PasswordInput'
import './UserService.scss'

export const CREATE_MODE = 'CREATE'
export const UPDATE_MODE = 'UPDATE'

const AddUserService = ({ userService, onSave, mode = CREATE_MODE }) => {
  const dispatch = useDispatch()
  const [form, setForm] = useState({})
  const [passVisibility, setPassVisibility] = useState(false)
  const handleChange = e => {
    setForm({ ...form, [e.target.name]: e.target.value })
  }
  const handleChangeVisibility = () => { console.log('asd')
    setPassVisibility(!passVisibility)
  }
  const handleSubmit = async e => {
    e.preventDefault()

    if (mode === UPDATE_MODE) form.id = userService.id

    const res = await dispatch(saveUserServiceAction(form, mode))

    if (res) {
      onSave && onSave()
      setForm({})
    }
  }

  useEffect(() => {
    if (userService) {
      const { username, password, service: { id: serviceId } } = userService
      setForm({ username, password, serviceId })
    }
  }, [userService])

  return (
    <form id="add-user-service-form" className='c-form-float userServiceItem' autoComplete='off' onSubmit={handleSubmit}>
      <SelectService
        name='serviceId'
        onChange={handleChange}
        value={form.serviceId || ''}
        disabled={mode === UPDATE_MODE}
      />
      <div className='c-form__input-group'>
        <input
          name='username'
          className='c-float-input'
          type='text'
          placeholder=' '
          value={form.username || ''}
          onChange={handleChange}
          required
        />
        <label className='c-float-label' htmlFor='username'>USERNAME</label>
      </div>
      <PasswordInput onChange={handleChange} value={form.password || ''} name="password" label="PASSWORD" />
      <Button type="submit">Save</Button>
    </form>
  )
}

export default AddUserService
