import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Select } from 'react-materialize'
import { getServicesAction } from '../../../../redux/ducks/services'

const SelectService = ({ onChange, value, name, disabled = false }) => {
  const dispatch = useDispatch()
  const { services } = useSelector(s => s.services)

  useEffect(() => {
    if (!services) {
      dispatch(getServicesAction())
    }
  }, [dispatch, services])

  const options = [
    <option key="0" disabled value="" >
      Choose a service
    </option>
  ]

  if (services) {
    for (const service of services) {
      options.push(<option key={service.id} value={service.id}>{service.service}</option>)
    }
  }

  return (
    <Select
      name={name}
      onChange={onChange}
      value={value.toString() || ''}
      disabled={disabled}
    >
      {options}
    </Select>
  )
}

export default SelectService
