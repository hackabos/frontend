import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { Button, Icon } from 'react-materialize'
import { deleteUserServiceAction } from '../../../../redux/ducks/services'
import { showModal, RELOAD_SERVICE_TYPE } from '../../../../redux/ducks/modals'
import SelectService from './SelectService'
import AddUserService, { UPDATE_MODE } from './AddUserService'
import './UserService.scss'

const UserServiceItem = ({ userService }) => {
  const dispatch = useDispatch()
  const [editing, setEditing] = useState(false)
  const handleEdit = e => {
    setEditing(true)
  }

  const handleDelete = () => dispatch(deleteUserServiceAction(userService.id))

  const onSave = () => setEditing(false)

  const openModal = () => dispatch(showModal(RELOAD_SERVICE_TYPE, { userServiceId: userService.id }))

  if (editing) return <AddUserService mode={UPDATE_MODE} userService={userService} onSave={onSave} />

  return (
    <div className="userServiceItem c-form-float">
      <SelectService value={userService.service.id || ''} disabled />
      <div className='c-form__input-group'>
        <input
          name='username'
          className='c-float-input'
          type='text'
          placeholder=' '
          value={userService.username || ''}
          disabled
        />
        <label className='c-float-label' htmlFor='username'>USERNAME</label>
      </div>
      <div className='c-form__input-group'>
        <input
          name='password'
          className='c-float-input'
          type='password'
          placeholder=' '
          value={userService.password || ''}
          disabled
        />
        <label className='c-float-label' htmlFor='username'>PASSWORD</label>
      </div>
      <Button className="u-btn-info" type="submit" onClick={handleEdit}>Edit</Button>
      <Button className="u-btn-error" type="submit" onClick={handleDelete}>Delete</Button>
      <div onClick={openModal}><Icon className="reload-service">autorenew</Icon></div>
    </div>
  )
}

export default UserServiceItem
