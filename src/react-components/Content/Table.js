/* eslint-disable react/display-name */
import React, { useEffect } from 'react'
import { Select } from 'react-materialize'
import { download, getBillsAction } from '../../redux/ducks/bills'
import { useDispatch, useSelector } from 'react-redux'
import {
  useTable,
  usePagination,
  useSortBy,
  useFilters,
  useGroupBy,
  useExpanded,
  useRowSelect
} from 'react-table'

import matchSorter from 'match-sorter'

const CustomSelectColumnFilter = ({
  column: { filterValue, setFilter, preFilteredRows, id }
}) => {
  const options = React.useMemo(() => {
    const options = new Set()
    preFilteredRows.forEach(row => {
      options.add(row.values[id])
    })
    return [...options.values()]
  }, [id, preFilteredRows])
  return (
    <Select
      options={{
        className: 'invoice_table-select'
      }}
      value={filterValue}
      onChange={e => {
        setFilter(e.target.value || undefined)
      }}
    >
      <option value={' '}>All</option>
      {options.map((option) => (
        <option key={Math.random()} value={option}>
          {option}
        </option>
      ))}
    </Select>
  )
}

function DefaultColumnFilter ({
  column: { filterValue, setFilter }
}) {
  return (
    <input
      className='invoice_table-input invoice_table-input-lg invoice_table-input--white'
      value={filterValue || ''}
      onChange={e => {
        setFilter(e.target.value || undefined)
      }}
      placeholder='Search'
    />
  )
}

function NumberRangeColumnFilter ({
  column: { filterValue = [], preFilteredRows, setFilter, id }
}) {
  React.useMemo(() => {
    let min = preFilteredRows.length ? preFilteredRows[0].values[id] : 0
    let max = preFilteredRows.length ? preFilteredRows[0].values[id] : 0
    preFilteredRows.forEach(row => {
      min = Math.min(row.values[id], min)
      max = Math.max(row.values[id], max)
    })
    return [min, max]
  }, [id, preFilteredRows])

  return (
    <div
      style={{
        display: 'flex'
      }}
    >
      <input
        className='invoice_table-input invoice_table-input-sm invoice_table-input--white invoice_table-input--hidden'
        value={filterValue[0] || ''}
        type='number'
        onChange={e => {
          const val = e.target.value
          setFilter((old = []) => [
            val ? parseInt(val, 10) : undefined,
            old[1]
          ])
        }}
        placeholder='Min'
      />
      <input
        autoFocus
        className='invoice_table-input invoice_table-input-sm invoice_table-input--white invoice_table-input--hidden'
        value={filterValue[1] || ''}
        type='number'
        onChange={e => {
          const val = e.target.value
          setFilter((old = []) => [
            old[0],
            val ? parseInt(val, 10) : undefined
          ])
        }}
        placeholder='Max'
      />
    </div>
  )
}

function fuzzyTextFilterFn (rows, id, filterValue) {
  return matchSorter(rows, filterValue, { keys: [row => row.values[id]] })
}

fuzzyTextFilterFn.autoRemove = val => !val

function Table ({ columns, data, skipReset }) {
  const dispatch = useDispatch()
  const filterTypes = React.useMemo(
    () => ({
      fuzzyText: fuzzyTextFilterFn,
      text: (rows, id, filterValue) => {
        return rows.filter(row => {
          const rowValue = row.values[id]
          return rowValue !== undefined
            ? String(rowValue)
              .toLowerCase()
              .startsWith(String(filterValue).toLowerCase())
            : true
        })
      }
    }),
    []
  )

  const defaultColumn = React.useMemo(
    () => ({
      Filter: DefaultColumnFilter
    }),
    []
  )

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    state: { pageIndex, pageSize, selectedRowIds }
  } = useTable(
    {
      columns,
      data,
      defaultColumn,
      filterTypes,
      autoResetPage: !skipReset,
      autoResetSelectedRows: !skipReset
    },
    useFilters,
    useGroupBy,
    useSortBy,
    useExpanded,
    usePagination,
    useRowSelect
  )

  const filterData = () => {
    const selectedRows = Object.keys(selectedRowIds)
    const selectedData = []
    for (const row of selectedRows) {
      selectedData.push(data[row])
    }

    return selectedData
  }

  filterData()

  const handleClick = (userServiceId, value, fileUrl) => () => {
    if (fileUrl) {
      window.open(fileUrl, '_blank')
    } else {
      dispatch(download(userServiceId, value))
    }
  }
  return (
    <>
      <div className='invoice-table'>
        <table {...getTableProps()} className='c-table c-table--bordered c-table--striped c-table--info'>
          <thead>
            {headerGroups.map((headerGroup, key) =>
              <tr key={key} {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column, key) =>
                  <th key={key} {...column.getHeaderProps()} >
                    <div>
                      <span {...column.getSortByToggleProps()} key={Math.random()}>
                        {column.render('Header')}
                        {column.isSorted
                          ? column.isSortedDesc
                            ? ' ▼'
                            : ' ▲'
                          : ''}
                      </span>
                    </div>
                    <div>{column.canFilter ? column.render('Filter') : null}</div>
                  </th>
                )}
              </tr>
            )}
          </thead>
          <tbody {...getTableBodyProps()}>
            {page.map(row => {
              prepareRow(row)
              return (
                <tr {...row.getRowProps()} key={Math.random()}>
                  {row.cells.map(cell => {
                    if (cell.column.Header === 'Invoice') {
                      return (
                        <td {...cell.getCellProps()}>
                          <span className='invoice-link' onClick={handleClick(cell.row.original.service.userServiceId
                            , cell.value, cell.row.original.file_url)} >
                            {cell.render('Cell')}
                            <i className="tiny material-icons">file_download</i>
                          </span>
                        </td>
                      )
                    }
                    if (cell.column.Header === 'Amount') {
                      return (
                        <td {...cell.getCellProps()}>
                          {parseFloat(cell.value).toFixed(2)}
                          {' €'}
                        </td>
                      )
                    }
                    if (cell.column.Header === 'Date') {
                      return (
                        <td className="invoice-date" {...cell.getCellProps()}>
                          {cell.value.substring(0, 10)}
                        </td>
                      )
                    }
                    if (cell.column.Header === 'Description') {
                      return (
                        <td {...cell.getCellProps()}>
                          {cell.value.join(', ')}
                        </td>
                      )
                    } else {
                      return (
                        <td {...cell.getCellProps()}>
                          {cell.render('Cell')}
                        </td>
                      )
                    }
                  })}
                </tr>
              )
            })}
          </tbody>
        </table>
        <div className='pagination-handler'>
          <button className='invoice_table-btn' onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
            {'<<'}
          </button>{' '}
          <button className='invoice_table-btn' onClick={() => previousPage()} disabled={!canPreviousPage}>
            {'<'}
          </button>{' '}
          <button className='invoice_table-btn' onClick={() => nextPage()} disabled={!canNextPage}>
            {'>'}
          </button>{' '}
          <button className='invoice_table-btn' onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
            {'>>'}
          </button>{' '}
          <span>
          Page{' '}
            <strong>
              {pageIndex + 1} of {pageOptions.length}
            </strong>{' '}
          </span>
          <span>| Go to:{' '} </span>
          <input
            className='invoice_table-input invoice_table-input-sm'
            type='number'
            defaultValue={pageIndex + 1}
            onChange={e => {
              const page = e.target.value ? Number(e.target.value) - 1 : 0
              gotoPage(page)
            }}
          />
          {' '}
          <span>Items per page:{' '}</span>
          <Select
            options={{
              className: 'invoice_table-select'
            }}
            value={pageSize.toString()}
            onChange={e => {
              setPageSize(Number(e.target.value))
            }}
          >
            {[10, 20, 30, 40, 50].map(pageSize => (
              <option key={Math.random()} value={pageSize}>
                {pageSize}
              </option>
            ))}
          </Select>
        </div>
        <div className='pagination-handler pagination-handler--collapsed'>
          <button className='invoice_table-btn' onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
            {'<<'}
          </button>{' '}
          <button className='invoice_table-btn' onClick={() => previousPage()} disabled={!canPreviousPage}>
            {'<'}
          </button>{' '}
          <button className='invoice_table-btn' onClick={() => nextPage()} disabled={!canNextPage}>
            {'>'}
          </button>{' '}
          <button className='invoice_table-btn' onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
            {'>>'}
          </button>{' '}
          <span>
          Page{' '}
            <strong>
              {pageIndex + 1} of {pageOptions.length}
            </strong>{' '}
          </span>
          <span>
          | Go to:{' '}
            <input
              type='number'
              className='invoice_table-input invoice_table-input-sm'
              defaultValue={pageIndex + 1}
              onChange={e => {
                const page = e.target.value ? Number(e.target.value) - 1 : 0
                gotoPage(page)
              }}
            />
          </span>{' '}
          <select
            key={Math.random()}
            value={pageSize}
            onChange={e => {
              setPageSize(Number(e.target.value))
            }}
          >
            {[10, 20, 30, 40, 50].map(pageSize => (
              <option key={Math.random()} value={pageSize}>
              Show {pageSize}
              </option>
            ))}
          </select>
        </div>
      </div>
    </>
  )
}

function filterGreaterThan (rows, id, filterValue) {
  return rows.filter(row => {
    const rowValue = row.values[id]
    return rowValue >= filterValue
  })
}

filterGreaterThan.autoRemove = val => typeof val !== 'number'

function InvoiceTable () {
  const dispatch = useDispatch()
  const bills = useSelector(s => s.bills)
  useEffect(() => {
    dispatch(getBillsAction())
  }, [dispatch])

  const columns = React.useMemo(
    () => [
      {
        Header: 'Invoice',
        accessor: 'number',
        Filter: '',
        filter: 'includes'
      },
      {
        Header: 'Service',
        accessor: 'service.service',
        Filter: CustomSelectColumnFilter,
        filter: 'includes'
      },
      {
        Header: 'Date',
        accessor: 'date',
        Filter: '',
        filter: 'includes'
      },
      {
        Header: 'Amount',
        accessor: 'amount',
        Filter: NumberRangeColumnFilter,
        filter: 'between',
        aggregate: 'sum'
      },
      {
        Header: 'Description',
        accessor: 'products',
        filter: 'fuzzyText'
      }
    ],
    []
  )

  const data = (bills && bills.billsData) || []
  return (
    <Table
      columns={columns}
      data={data}
    />
  )
}

export default InvoiceTable
