import React, { useEffect } from 'react'
import { useSelector } from 'react-redux'
import { Route, Switch, useHistory } from 'react-router-dom'
import Signup from './Account/Signup/Signup'
import Login from './Account/Login/Login'
import PassRecovery from './Account/PassRecovery/PassRecovery'

const Public = () => {
  const { token } = useSelector(s => s.user)
  const history = useHistory()

  useEffect(() => {
    if (!token) {
      history.push('/login')
    }
  }, [token, history])

  return (
    <Switch>
      <Route path="/signup">
        <Signup />
      </Route>
      <Route path="/login">
        <Login />
      </Route>
      <Route path="/forgot-password">
        <PassRecovery />
      </Route>
    </Switch>
  )
}

export default Public
