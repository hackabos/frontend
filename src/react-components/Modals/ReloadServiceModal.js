import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { DatePicker, Checkbox } from 'react-materialize'
import moment from 'moment'
import { hideModal, RELOAD_SERVICE_TYPE } from '../../redux/ducks/modals'
import { reloadServiceAction } from '../../redux/ducks/services'
import Modal from './Modal'

const ReloadServiceModal = () => {
  const dispatch = useDispatch()
  const { modalType, data } = useSelector(s => s.modals)
  const [reloadAll, setReloadAll] = useState(false)
  const DATE_FORMAT = 'MMM DD, YYYY'
  const defaultDate = moment().subtract(1, 'month').format(DATE_FORMAT)
  const [date, setDate] = useState(defaultDate)
  const handleChange = e => setReloadAll(e.target.checked)
  const handleDate = date => setDate(moment(date).format(DATE_FORMAT))
  const handleSubmit = () => {
    if (!data.userServiceId) return false

    if (reloadAll) {
      dispatch(reloadServiceAction(data.userServiceId))
    } else {
      dispatch(reloadServiceAction(data.userServiceId, moment(date, DATE_FORMAT).format('YYYY-MM-DD')))
    }

    dispatch(hideModal())
  }

  if (modalType !== RELOAD_SERVICE_TYPE) return false

  return (
    <Modal
      title="RELOAD BILLS"
      footer ={<button onClick={handleSubmit} className="c-btn c-btn--md c-btn--full-width u-btn-info"> RELOAD </button>}
    >
      <label>Reload from date:</label>
      <DatePicker
        options={{
          firstDay: 1,
          defaultDate: defaultDate,
          setDefaultDate: true
        }}
        disabled={reloadAll}
        value={date}
        onChange={handleDate}
      />
      <Checkbox
        label="Reload All"
        checked={reloadAll}
        onChange={handleChange}
        value=""
      />
    </Modal>
  )
}

export default ReloadServiceModal
