import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { hideModal } from '../../redux/ducks/modals'

const Modal = ({ title, children, footer, dismissible = true }) => {
  const dispatch = useDispatch()
  const modalState = useSelector(s => s.modals)

  const handleClick = () => {
    if (!dismissible) return

    dispatch(hideModal())
  }

  if (!modalState.showing) return false

  return (
    <div className="c-modal">
      <article className="o-card u-box--shadow-dark c-login-card">
        <div className="o-card__inner">
          <header className="o-card__header">
            <h1>{title}</h1>
          </header>
          <main className="o-card__body">{children}</main>
          <footer className="o-card__footer">{footer}</footer>
        </div>
      </article>
      <div className="modal-bg" onClick={handleClick}></div>
    </div>
  )
}

export default Modal
