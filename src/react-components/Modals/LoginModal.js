import React from 'react'
import { useSelector } from 'react-redux'
import { LOGIN_TYPE } from '../../redux/ducks/modals'
import Modal from './Modal'
import LoginForm from '../Login/LoginForm'

const LoginModal = () => {
  const { modalType } = useSelector(s => s.modals)

  if (modalType !== LOGIN_TYPE) return false

  return (
    <Modal
      dismissible={false}
      title="LOGIN REQUIRED"
      footer ={<button className="c-btn c-btn--md c-btn--full-width u-btn-info" type="submit" form="login-form"> LOGIN </button>}
    >
      <LoginForm />
    </Modal>
  )
}

export default LoginModal
