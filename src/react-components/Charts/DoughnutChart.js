/* eslint-disable no-unused-expressions */
import React from 'react'
import { Doughnut } from 'react-chartjs-2'

const transformData = (arr) => {
  const counts = {}
  for (const invoice of arr) {
    const service = invoice.service.service
    // eslint-disable-next-line no-prototype-builtins
    if (!counts.hasOwnProperty(service)) {
      counts[service] = 1
    } else {
      counts[service]++
    }
  }
  const labels = Object.keys(counts)
  const data = Object.values(counts)
  const dataSet = {
    labels,
    datasets: [
      {
        label: 'Population',
        data,
        backgroundColor: [
          'rgba(227,180,50,1)',
          'rgba(122,163,135,1)',
          'rgba(19,22,66,1)',
          'rgba(80,83,87,1)',
          'rgba(237,142,78,1)',
          'rgba(169,136,184,1)',
          'rgba(168,171,173,1)',
          'rgba(61,120,122,1)',
          'rgba(173,101,90,1)',
          'rgba(60,92,64,1)',
          'rgba(148,104,59,1)'
        ]
      }
    ]
  }

  return dataSet
}

const options = {
  tooltips: {
    callbacks: {
      label: function (tooltipItem, data) {
        const dataset = data.datasets[tooltipItem.datasetIndex]
        const meta = dataset._meta[Object.keys(dataset._meta)[0]]
        const total = meta.total
        const currentValue = dataset.data[tooltipItem.index]
        const percentage = parseFloat((currentValue / total * 100).toFixed(1))
        if (currentValue === 1) {
          return `${currentValue} order (${percentage}%)`
        } else {
          return `${currentValue} orders (${percentage}%)`
        }
      },
      title: function (tooltipItem, data) {
        return data.labels[tooltipItem[0].index]
      }
    }
  },
  legend: {
    display: true,
    position: 'left'
  },
  responsive: false,
  maintainAspectRatio: false,
  scales: {
    xAxes: [{
      display: false
    }],
    yAxes: [{
      display: false,
      drawBorder: false,
      ticks: {
        beginAtZero: true,
        userCallback: function (label) {
          if (Math.floor(label) === label) {
            return label
          }
        },
        suggestedMin: 1,
        suggestedMax: 2
      }
    }]
  }
}

const DoughnutChart = (props) => {
  return (
    <>
      <div className="chart-item">
        <div className="chart-title">Nº of orders</div>
        <div>
          <Doughnut
            data= {transformData(props.data)}
            options={options}
          />
        </div>
      </div>
    </>
  )
}

export default DoughnutChart
