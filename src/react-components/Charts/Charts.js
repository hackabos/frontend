import React, { useEffect, useState } from 'react'
import { getBillsAction } from '../../redux/ducks/bills'
import { useDispatch, useSelector } from 'react-redux'
import { Select } from 'react-materialize'
import moment from 'moment'
import DoughnutChart from './DoughnutChart'
import BarChart from './BarChart'
import LineChart from './LineChart'

const hasValues = values => values.length === 0
const noAllSelectedAndNotInValues = (state, values) => state.indexOf('All') === -1 && values.indexOf('All') !== -1
const isEverySelected = (values, data) => {
  const valuesWithoutAll = values.filter(v => v !== 'All')
  const dataWithoutAll = data.filter(d => d !== 'All')
  return valuesWithoutAll.length === dataWithoutAll.length
}

const Charts = () => {
  const dispatch = useDispatch()
  const { billsData } = useSelector(s => s.bills)
  let dataForChart = [...billsData]
  const services = ['All']
  const years = []
  const [selectedServices, setSelectedServices] = useState(['All'])
  const [selectedYears, setSelectedYears] = useState(['All'])
  const [maxAmount, setMaxAmount] = useState('')
  const [minAmount, setMinAmount] = useState('')

  const handleChangeSelect = (data, state, setState) => e => {
    const options = e.target.options
    let values = []

    for (const option of options) {
      if (option.selected && !option.disabled) {
        values.push(option.value)
      }
    }
    if (hasValues(values) || noAllSelectedAndNotInValues(state, values) || isEverySelected(values, data)) {
      setState(['All'])
      return
    }
    if (!isEverySelected(values, data) && values.indexOf('All') !== -1) {
      values = values.filter(v => v !== 'All')
    }

    setState(values)
  }

  const handleChangeAmount = (state, setState) => e => {
    setState(e.target.value)
  }

  // Filter data
  if (selectedServices.indexOf('All') === -1) {
    dataForChart = dataForChart.filter(data => selectedServices.indexOf(data.service.service) !== -1)
  }
  if (selectedYears.indexOf('All') === -1) {
    dataForChart = dataForChart.filter(data => selectedYears.indexOf(moment(data.date).format('YYYY')) !== -1)
  }
  if (maxAmount) {
    dataForChart = dataForChart.filter(data => parseFloat(data.amount) <= maxAmount)
  }
  if (minAmount) {
    dataForChart = dataForChart.filter(data => parseFloat(data.amount) >= minAmount)
  }

  // Initialize services and years
  if (billsData) {
    for (const bill of billsData) {
      if (services.indexOf(bill.service.service) === -1) {
        services.push(bill.service.service)
      }

      const year = moment(bill.date).format('YYYY')
      if (years.indexOf(year) === -1) {
        years.push(year)
      }
    }

    years.sort()
    years.unshift('All')
  }

  useEffect(() => {
    dispatch(getBillsAction())
  }, [dispatch])

  return (
    <section className='charts-container'>
      <section className='charts-filters'>
        <div className='charts-filters-wrapper'>
          <p className='charts-filters-tittle'>Services</p>
          <Select
            multiple
            value={selectedServices}
            onChange={handleChangeSelect(services, selectedServices, setSelectedServices)}
          >
            {services.map(service => (
              <option key={service} value={service}>
                {service}
              </option>
            ))}
          </Select>
        </div>
        <div className='charts-filters-wrapper'>
          <p className='charts-filters-tittle'>Years</p>
          <Select
            multiple
            value={selectedYears}
            onChange={handleChangeSelect(years, selectedYears, setSelectedYears)}
          >
            {years.map(year => (
              <option key={year} value={year}>
                {year}
              </option>
            ))}
          </Select>
        </div>
        <div className='charts-filters-wrapper'>
          <p className='charts-filters-tittle charts-filters-tittle--bottom'>Amount</p>
          <input
            className='charts-filters-input '
            type='number'
            placeholder='Min'
            value={minAmount}
            onChange={handleChangeAmount(minAmount, setMinAmount)}
          />
          <input
            className='charts-filters-input'
            type='number'
            placeholder='Max'
            value={maxAmount}
            onChange={handleChangeAmount(maxAmount, setMaxAmount)}
          />
        </div>
      </section>
      <article className="chart-item chart-item-1">
        <BarChart data={dataForChart} />
      </article>
      <article className="chart-item chart-item-2">
        <LineChart data={dataForChart} />
      </article>
      <article className="chart-item chart-item-3">
        <DoughnutChart data={dataForChart} />
      </article>
    </section>
  )
}

export default Charts
