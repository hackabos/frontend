/* eslint-disable no-unused-expressions */
import React from 'react'
import { Bar } from 'react-chartjs-2'

const transformData = arr => {
  const counts = {}
  for (const invoice of arr) {
    const year = new Date(invoice.date).getFullYear()
    // eslint-disable-next-line no-prototype-builtins
    if (!counts.hasOwnProperty(year)) {
      counts[year] = invoice.amount * 1
    } else {
      counts[year] += invoice.amount * 1
    }
  }
  const labels = Object.keys(counts)
  const data = Object.values(counts)
  const dataSet = {
    labels,
    datasets: [
      {
        label: 'Total Spending',
        data,
        backgroundColor: 'rgba(237,142,78,1)'
      }
    ]
  }
  return dataSet
}

const options = {
  tooltips: {
    callbacks: {
      label: function (tooltipItem, data) {
        const dataset = data.datasets[tooltipItem.datasetIndex]
        const currentValue = dataset.data[tooltipItem.index].toFixed(2)
        return `${currentValue} €`
      },
      title: function (tooltipItem, data) {
        return data.labels[tooltipItem[0].index]
      }
    }
  },
  legend: {
    display: false
  },
  responsive: true,
  maintainAspectRatio: false,
  scales: {
    xAxes: [{
      display: true
    }],
    yAxes: [{
      display: true,
      drawBorder: true,
      ticks: {
        beginAtZero: true,
        userCallback: function (label) {
          if (Math.floor(label) === label) {
            return label
          }
        },
        suggestedMin: 1,
        suggestedMax: 2
      }
    }]
  }
}

const BarChart = (props) => {
  return (
    <>
      <div className="chart-item">
        <div className="chart-title">Anual total spending</div>
        <div>
          <Bar
            data= {transformData(props.data)}
            options={options}
          />
        </div>
      </div>
    </>
  )
}

export default BarChart
