/* eslint-disable no-unused-expressions */
import React from 'react'
import { HorizontalBar } from 'react-chartjs-2'

const rawData = [
  {
    number: 'ac123456',
    date: '2019-12-31T23:00:00.000Z',
    amount: '152.45',
    service: {
      service: 'Amazon'
    }
  },
  {
    number: 'er098098',
    date: '2019-12-31T23:00:00.000Z',
    amount: '23',
    service: {
      service: 'Amazon'
    }
  },
  {
    number: 'qwe90ew1',
    date: '2020-02-29T23:00:00.000Z',
    amount: '238.2',
    service: {
      service: 'R Telefonía'
    }
  },
  {
    number: 'qwe90ew1',
    date: '2018-02-29T23:00:00.000Z',
    amount: '120.2',
    service: {
      service: 'R Telefonía'
    }
  },
  {
    number: 'qwe90ew1',
    date: '2015-02-02T16:00:00.000Z',
    amount: '120.2',
    service: {
      service: 'Otro'
    }
  }
]

const transformData = arr => {
  const counts = {}
  for (const invoice of arr) {
    const year = new Date(invoice.date).getFullYear()
    // eslint-disable-next-line no-prototype-builtins
    if (!counts.hasOwnProperty(year)) {
      counts[year] = invoice.amount * 1
    } else {
      counts[year] += invoice.amount * 1
    }
  }

  const labels = Object.keys(counts)
  const data = Object.values(counts)
  const dataSet = {
    labels,
    datasets: [
      {
        label: 'Total Spending',
        data,
        backgroundColor: [
          'rgba(12,24,39,1)',
          'rgba(14,69,128,0.8)',
          'rgba(0, 100, 148,1)'
        ]
      }
    ]
  }

  return dataSet
}

const options = {
  tooltips: {
    callbacks: {
      label: function (tooltipItem, data) {
        const dataset = data.datasets[tooltipItem.datasetIndex]
        const currentValue = dataset.data[tooltipItem.index]
        return `${currentValue} €`
      },
      title: function (tooltipItem, data) {
        return data.labels[tooltipItem[0].index]
      }
    }
  },
  legend: {
    display: false
  },
  maintainAspectRatio: false,
  scales: {
    xAxes: [{
      display: true
    }],
    yAxes: [{
      display: true,
      drawBorder: true,
      ticks: {
        beginAtZero: true,
        userCallback: function (label) {
          if (Math.floor(label) === label) {
            return label
          }
        },
        suggestedMin: 1,
        suggestedMax: 2
      }
    }]
  }
}

const HorizontalBarChart = () => {
  return (
    <>
      <div className="chart-item">
        <div className="chart-title">Anual total spending</div>
        <div>
          <HorizontalBar
            data= {transformData(rawData)}
            options={options}
          />
        </div>
      </div>
    </>
  )
}

export default HorizontalBarChart
