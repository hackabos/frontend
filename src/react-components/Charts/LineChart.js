import React from 'react'
import { Line } from 'react-chartjs-2'
import moment from 'moment'
const options = {
  tooltips: {
    callbacks: {
      label: function (tooltipItem, data) {
        const dataset = data.datasets[tooltipItem.datasetIndex]
        const currentValue = dataset.data[tooltipItem.index].toFixed(2)
        return `${currentValue} €`
      },
      title: function (tooltipItem, data) {
        return data.labels[tooltipItem[0].index]
      }
    }
  },
  legend: {
    display: false
  },
  responsive: true,
  maintainAspectRatio: false,
  scales: {
    xAxes: [{
      display: true
    }],
    yAxes: [{
      display: true,
      drawBorder: true,
      ticks: {
        beginAtZero: true,
        userCallback: function (label) {
          if (Math.floor(label) === label) {
            return label
          }
        },
        suggestedMin: 1,
        suggestedMax: 2
      }
    }]
  }
}

const transformData = data => {
  const services = {}
  const allYears = []
  const serviceNames = []

  for (const invoice of data) {
    const serviceName = invoice.service.service
    const year = moment(invoice.date).format('YYYY')
    if (!services[serviceName]) {
      services[serviceName] = {}
    }
    if (!services[serviceName][year]) {
      services[serviceName][year] = 0
    }
    services[serviceName][year] += parseFloat(invoice.amount)

    if (allYears.indexOf(year) === -1) allYears.push(year)
    if (serviceNames.indexOf(serviceName) === -1) serviceNames.push(serviceName)
  }

  allYears.sort()
  const labels = []
  for (let i = allYears[0]; i <= allYears[allYears.length - 1]; i++) {
    labels.push(i)
  }
  const colors = [
    'rgba(227,180,50,1)',
    'rgba(122,163,135,1)',
    'rgba(19,22,66,1)',
    'rgba(80,83,87,1)',
    'rgba(237,142,78,1)',
    'rgba(169,136,184,1)',
    'rgba(168,171,173,1)',
    'rgba(61,120,122,1)',
    'rgba(173,101,90,1)',
    'rgba(60,92,64,1)',
    'rgba(148,104,59,1)'
  ]
  const result = { labels, datasets: [] }
  let colorIndex = 0
  for (const serviceName of serviceNames) {
    const serviceData = services[serviceName]
    const toPush = {
      label: serviceName,
      data: [],
      fill: false,
      borderWidth: 2,
      borderColor: colors[colorIndex]
    }
    toPush.pointColor = toPush.borderColor
    colorIndex += 1
    for (const year of labels) {
      if (!serviceData[year]) {
        toPush.data.push(0)
      } else {
        toPush.data.push(serviceData[year])
      }
    }
    result.datasets.push(toPush)
  }

  result.datasets.borderColor = '#ff6c23'
  return result
}

const LineChart = (props) => {
  return (
    <>
      <div className="chart-item">
        <div className="chart-title">Anual spending per service</div>
        <div>
          <Line
            data= {transformData(props.data)}
            options={options}
          />
        </div>
      </div>
    </>

  )
}

export default LineChart
