import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { loginAction } from '../../../redux/ducks/user'
import PasswordInput from '../../Utils/PasswordInput'

const LoginForm = () => {
  const dispatch = useDispatch()
  const [form, setForm] = useState({ username: 'admin@user.com', password: '123456789' })
  const handleChange = e => {
    setForm({ ...form, [e.target.id]: e.target.value })
  }
  const handleCheckbox = e => {
    setForm({ ...form, remember: e.target.checked })
  }
  const handleSubmit = e => {
    e.preventDefault()

    dispatch(loginAction(form))
  }

  return (
    <form id="login-form" className='c-form-float' onSubmit={handleSubmit}>
      <div className='c-form__input-group'>
        <input
          id='username'
          className='c-float-input'
          type='text'
          placeholder=' '
          value={form.username || ''}
          onChange={handleChange}
        />
        <label className='c-float-label' htmlFor='username'>EMAIL</label>
      </div>
      <PasswordInput value={form.password || ''} onChange={handleChange} id='password' label="PASSWORD" />
      <div className='c-form__input-group'>
        <label>
          <input type='checkbox' onChange={handleCheckbox}/>
          <span className='u-text-sm'>REMEMBER ME</span>
        </label>
      </div>
      <Link to="/forgot-password" className='u-text-sm'>FORGOT PASSWORD?</Link>
    </form>
  )
}

export default LoginForm
