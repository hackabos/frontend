import React from 'react'
import { Link } from 'react-router-dom'
import LoginForm from './LoginForm'

const Login = ({ onNeedSignup }) => {
  return (
    <div className='u-content-centered-XY'>
      <section className='o-card u-box--shadow c-login-card'>
        <div className='o-card__inner'>
          <div className='o-card__header'>
            <h1>ACCOUNT LOGIN</h1>
          </div>
          <div className='o-card__body'>
            <LoginForm />
          </div>
          <div className='o-card__footer u-flex-column'>
            <button className='c-btn c-btn--md c-btn--full-width u-btn-info' type='submit' form="login-form"> LOG IN </button>
            <Link to="/signup" className='u-text-sm u-margin-top' onClick={onNeedSignup}>CREATE ACCOUNT</Link>
          </div>
        </div>
      </section>
    </div>
  )
}

export default Login
