import React from 'react'
import { Link } from 'react-router-dom'
import SignupFormForm from './SignupForm'

const SignupFormCard = ({ onSignupSuccess }) => {
  return (
    <div className='u-content-centered-XY'>
      <section className='o-card u-box--shadow c-login-card'>
        <div className='o-card__inner'>
          <div className='o-card__header'>
            <h1>CREATE ACCOUNT</h1>
          </div>
          <div className='o-card__body'>
            <SignupFormForm onSignupSuccess={onSignupSuccess}/>
          </div>
          <div className='o-card__footer u-flex-column'>
            <button className='c-btn c-btn--md c-btn--full-width u-btn-info' type='submit' form="login-form"> CREATE </button>
            <Link to="/login" className='u-text-sm u-margin-top' onClick={onSignupSuccess}>ALREADY HAVE AN ACCOUNT?</Link>
          </div>
        </div>
      </section>
    </div>
  )
}

export default SignupFormCard
