import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { signupAction } from '../../../redux/ducks/user'
import PasswordInput from '../../Utils/PasswordInput'

const SignupForm = () => {
  const dispatch = useDispatch()
  const [form, setForm] = useState({})
  const handleChange = e => {
    setForm({ ...form, [e.target.id]: e.target.value })
  }
  const handleSubmit = async e => {
    e.preventDefault()

    dispatch(signupAction(form))
  }

  return (
    <form id="login-form" className='c-form-float' onSubmit={handleSubmit}>
      <div className='c-form__input-group'>
        <input
          id='username'
          className='c-float-input'
          type='text'
          placeholder=' '
          value={form.username || ''}
          onChange={handleChange}
        />
        <label className='c-float-label' htmlFor='username'>EMAIL</label>
      </div>
      <PasswordInput value={form.password || ''} onChange={handleChange} id='password' label="PASSWORD" />
      <PasswordInput value={form['re-password'] || ''} onChange={handleChange} id='re-password' label="RE-TYPE PASSWORD" />
    </form>
  )
}

export default SignupForm
