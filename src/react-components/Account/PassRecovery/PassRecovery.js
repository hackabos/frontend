import React from 'react'
import { Link } from 'react-router-dom'
import PassRecoveryForm from './PassRecoveryForm'

const PassRecovery = () => {
  return (
    <div className='u-content-centered-XY'>
      <section className='o-card u-box--shadow c-login-card'>
        <div className='o-card__inner'>
          <div className='o-card__header'>
            <h1>RECOVER PASSWORD</h1>
          </div>
          <div className='o-card__body'>
            <PassRecoveryForm />
          </div>
          <div className='o-card__footer u-flex-column'>
            <button className='c-btn c-btn--md c-btn--full-width u-btn-info' type='submit' form="login-form"> RECOVER </button>
            <Link to="/login" className='u-text-sm u-margin-top'>ALREADY HAVE AN ACCOUNT?</Link>
            <Link to="/login" className='u-text-sm u-margin-top'>CREATE ACCOUNT</Link>
          </div>
        </div>
      </section>
    </div>
  )
}

export default PassRecovery
