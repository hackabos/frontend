import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { passRecoveryAction } from '../../../redux/ducks/user'

const PassRecovery = () => {
  const dispatch = useDispatch()
  const [form, setForm] = useState({})
  const handleChange = e => {
    setForm({ ...form, [e.target.id]: e.target.value })
  }
  const handleSubmit = async e => {
    e.preventDefault()

    dispatch(passRecoveryAction(form))
  }

  return (
    <form id="login-form" className='c-form-float' onSubmit={handleSubmit}>
      <div className='c-form__input-group'>
        <input
          id='username'
          className='c-float-input'
          type='text'
          placeholder=' '
          value={form.username || ''}
          onChange={handleChange}
        />
        <label className='c-float-label' htmlFor='username'>EMAIL</label>
      </div>
    </form>
  )
}

export default PassRecovery
