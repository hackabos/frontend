import React from 'react'

const Footer = () => {
  return (
    <footer className='c-layout-dashboard__footer'>
      @ {new Date().getFullYear()} BillBox
    </footer>
  )
}

export default Footer
