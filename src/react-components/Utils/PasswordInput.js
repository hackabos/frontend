import React, { useState } from 'react'
import { Icon } from 'react-materialize'

const PasswordInput = ({ value, onChange, onKeyUp, name, id, label, children }) => {
  const [passVisibility, setPassVisibility] = useState(false)
  const handleChangeVisibility = () => {
    setPassVisibility(!passVisibility)
  }
  return (
    <div className='c-form__input-group'>
      <input
        id={id}
        name={name}
        className='c-float-input'
        type={passVisibility ? 'text' : 'password'}
        placeholder=' '
        value={value || ''}
        onChange={onChange}
        onKeyUp={onKeyUp}
        required
      />
      <div className="c-form__input-icon" onClick={handleChangeVisibility}><Icon>{passVisibility ? 'visibility' : 'visibility_off'}</Icon></div>
      <label className='c-float-label' htmlFor='password'>{label}</label>
      {children}
    </div>
  )
}

export default PasswordInput
