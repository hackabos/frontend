import React from 'react'
import SideBarMenu from './SideBarMenu'

const LeftSidebar = () => {
  return (
    <aside className="c-layout-dashboard__aside u-show-from--md">
      <header className="c-navbar__logo"><div className="c-navbar__logo-img"></div></header>
      <main>
        <SideBarMenu />
      </main>
    </aside>
  )
}

export default LeftSidebar
