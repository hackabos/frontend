import React from 'react'
import { NavLink } from 'react-router-dom'
import invoiceSVG from '../../img/invoice.svg'
import barChartSVG from '../../img/bar-chart.svg'
import './SideBarMenu.scss'

const SideBarMenu = () => {
  return (
    <ul className="SideBarMenu">
      <li className="c-nav__item">
        <NavLink
          className="c-nav__item-link"
          to="/bills"
          isActive={(match, location) => {
            if (match || location.pathname === '/') return true
            return false
          }}
        >
          <img src={invoiceSVG} />
          <span className="u-hide--lg u-show--md">Bills</span>
        </NavLink>
      </li>
      <li className="c-nav__item">
        <NavLink className="c-nav__item-link" to="/charts">
          <img src={barChartSVG} />
          <span className="u-hide--lg u-show--md">Charts</span>
        </NavLink>
      </li>
    </ul>
  )
}

export default SideBarMenu
