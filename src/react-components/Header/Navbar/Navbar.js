import React from 'react'
import { NavLink } from 'react-router-dom'
import { Dropdown, Divider, Icon } from 'react-materialize'
import { useDispatch, useSelector } from 'react-redux'
import { logoutAction } from '../../../redux/ducks/user'
import Notifications from './Notifications/Notifications'
import './Navbar.scss'

const Navbar = () => {
  const dispatch = useDispatch()
  const { profile: { username } } = useSelector(s => s.user)
  const handleClick = () => {
    dispatch(logoutAction())
  }

  return (
    <nav className="Navbar c-layout-dashboard__header c-navbar o-flex-box o-flex-box--cross-center">
      <div data-target="slide-out" className="sidenav-trigger c-sidenav__trigger u-hide--md">
        <i className="material-icons">menu</i>
      </div>
      <section className="o-flex-box__right">
        <ul className="o-flex-box">
          <li className="c-nav__item">
            <Notifications className="c-nav__item-link" />
          </li>
          <li className="c-nav__item">
            <Dropdown trigger={<div className="c-nav__item-link"><Icon>account_circle</Icon>{' ' + username || ''}</div>} >
              <NavLink to="/profile">Profile</NavLink>
              <NavLink to="/settings">Settings</NavLink>
              <Divider />
              <NavLink to="/" onClick={handleClick}>Log out</NavLink>
            </Dropdown>
          </li>
        </ul>
      </section>
    </nav>
  )
}

export default Navbar
