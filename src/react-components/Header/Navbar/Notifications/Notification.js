import React from 'react'
import { Divider } from 'react-materialize'
import moment from 'moment'

const Notification = ({ notification }) => {
  return (
    <>
      <p>{notification.message}</p>
      <span>{moment(notification.createdAt).format('DD-MM-YYYY')}</span>
      <Divider />
    </>
  )
}

export default Notification
