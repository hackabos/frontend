import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Dropdown } from 'react-materialize'
import { getNotificationsAction, deleteNotificationsAction } from '../../../../redux/ducks/notifications'
import Notification from './Notification'
import './Notifications.scss'

const Notifications = ({ className }) => {
  const dispatch = useDispatch()
  const { notificationsData } = useSelector(s => s.notifications)
  const handleClose = () => {
    dispatch(deleteNotificationsAction())
  }

  useEffect(() => {
    dispatch(getNotificationsAction())

    const t = setInterval(() => {
      dispatch(getNotificationsAction())
    }, 1000 * 10)

    return () => {
      clearInterval(t)
    }
  }, [dispatch])

  if (!notificationsData.length) return <i className={`material-icons ${className}`} >notifications</i>

  return (
    <Dropdown
      className={`Notifications ${className}`}
      trigger={
        <div className="NotificationsIcon c-nav__item-link">
          <i className="material-icons">notifications</i>
          <span className="notifications-badge">{notificationsData.length}</span>
        </div>
      }
      options={{ onCloseStart: handleClose }}
    >
      <div className="NotificationsPanel">
        <h1>NOTIFICATIONS</h1>
        <ul>
          {notificationsData.map(notification =>
            <li key={notification.id}>
              <Notification notification={notification} />
            </li>
          )}
        </ul>
      </div>
    </Dropdown>
  )
}

export default Notifications
