import React, { useRef } from 'react'
import SideBarMenu from '../LeftSideBar/SideBarMenu'
import './HamMenu.scss'

const HamMenu = () => {
  const sideMenuEl = useRef(null)

  if (sideMenuEl.current) {
    window.M.Sidenav.init(sideMenuEl.current)
  }

  return (
    <div id="slide-out" className="sidenav" ref={sideMenuEl}>
      <header className="c-navbar__logo-img"></header>
      <main>
        <SideBarMenu />
      </main>
    </div>
  )
}

export default HamMenu
