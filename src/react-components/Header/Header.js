import React from 'react'
import Navbar from './Navbar/Navbar'
import HamMenu from './HamMenu'

const Header = () => {
  return (
    <>
      <Navbar/>
      <HamMenu/>
    </>
  )
}

export default Header
