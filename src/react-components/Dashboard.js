import React, { useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import Header from './Header/Header'
import LeftSideBar from './LeftSideBar/LeftSidebar'
import Content from './Content/Content'
import Footer from './Footer'
import '../scss/main.scss'

const Dashboard = () => {
  const history = useHistory()

  useEffect(() => {
    history.push('/')
  }, [history])

  return (
    <div className="c-body">
      <div className="c-layout-dashboard">
        <Header />
        <LeftSideBar />
        <Content />
        <Footer />
      </div>
    </div>
  )
}

export default Dashboard
