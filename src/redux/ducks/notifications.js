import { logoutAction } from './user'
import { getBillsAction } from './bills'

const initialSate = {
  fetching: false,
  notificationsData: []
}

const GET_NOTIFICATIONS = 'GET_NOTIFICATIONS'
const GET_NOTIFICATIONS_SUCCESS = 'GET_NOTIFICATIONS_SUCCESS'
const GET_NOTIFICATIONS_ERROR = 'GET_NOTIFICATIONS_ERROR'

const DELETE_NOTIFICATIONS = 'DELETE_NOTIFICATIONS'
const DELETE_NOTIFICATIONS_SUCCESS = 'DELETE_NOTIFICATIONS_SUCCESS'

// reducer
export default function reducer (state = initialSate, action) {
  switch (action.type) {
    case GET_NOTIFICATIONS:
      return { ...state, fetching: true, error: false }
    case GET_NOTIFICATIONS_SUCCESS:
      return { ...state, fetching: false, notificationsData: action.payload.notifications, error: false }
    case GET_NOTIFICATIONS_ERROR:
      return { ...state, fetching: false, error: action.payload.error }

    case DELETE_NOTIFICATIONS:
      return { ...state, fetching: true, error: false }
    case DELETE_NOTIFICATIONS_SUCCESS:
      return { ...state, fetching: false, notificationsData: [] }
    default:
      return state
  }
}

// actions
export const getNotificationsAction = () => async (dispatch, getState) => {
  let error
  const { user: { token } } = getState()

  dispatch({ type: GET_NOTIFICATIONS })

  try {
    const data = await fetch(process.env.REACT_APP_API_URL + '/notifications', {
      method: 'GET',
      headers: {
        'content-Type': 'application/json',
        Authorization: 'bearer ' + token
      }
    })

    const body = await data.json()

    if (data.status === 200) {
      dispatch({
        type: GET_NOTIFICATIONS_SUCCESS,
        payload: { notifications: body }
      })

      if (body.length) {
        dispatch(getBillsAction())
      }
      return
    }
    if (data.status === 401) {
      dispatch(logoutAction())
      return
    }

    error = JSON.stringify(body.errors[0]).replace(/{|}|"/g, ' ').trim()
  } catch (er) {
    error = er
  }

  dispatch({
    type: GET_NOTIFICATIONS_ERROR,
    payload: { error }
  })

  window.M.toast({ html: error })
}

export const deleteNotificationsAction = () => async (dispatch, getState) => {
  const { user: { token } } = getState()

  dispatch({ type: DELETE_NOTIFICATIONS })

  try {
    const data = await fetch(process.env.REACT_APP_API_URL + '/notifications', {
      method: 'DELETE',
      headers: {
        'content-Type': 'application/json',
        Authorization: 'bearer ' + token
      }
    })

    if (data.status === 200) {
      dispatch({
        type: DELETE_NOTIFICATIONS_SUCCESS
      })
      return
    }
    if (data.status === 401) {
      dispatch(logoutAction())
      return
    }
  } catch (er) {
    console.log(er)
  }
}
