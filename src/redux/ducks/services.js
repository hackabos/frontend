import { logoutAction } from './user'

const initialSate = {
  fetching: false,
  services: null,
  userServices: []
}

const GET_SERVICES = 'GET_SERVICES'
const GET_SERVICES_SUCCESS = 'GET_SERVICES_SUCCESS'
const GET_SERVICES_ERROR = 'GET_SERVICES_ERROR'

const GET_USER_SERVICES = 'GET_USER_SERVICES'
const GET_USER_SERVICES_SUCCESS = 'GET_SERVICES_USER_SUCCESS'
const GET_USER_SERVICES_ERROR = 'GET_SERVICES_USER_ERROR'

const SAVE_USER_SERVICES = 'SAVE_USER_SERVICES'
const SAVE_USER_SERVICES_SUCCESS = 'SAVE_SERVICES_USER_SUCCESS'
const SAVE_USER_SERVICES_ERROR = 'SAVE_SERVICES_USER_ERROR'

const DELETE_USER_SERVICES = 'DELETE_USER_SERVICES'
const DELETE_USER_SERVICES_SUCCESS = 'DELETE_SERVICES_USER_SUCCESS'
const DELETE_USER_SERVICES_ERROR = 'DELETE_SERVICES_USER_ERROR'

const RELOAD_SERVICE = 'RELOAD_SERVICE'
const RELOAD_SERVICE_SUCCESS = 'RELOAD_SERVICE_SUCCESS'
const RELOAD_SERVICE_ERROR = 'RELOAD_SERVICE_ERROR'

// reducer
export default function reducer (state = initialSate, action) {
  switch (action.type) {
    case GET_SERVICES:
      return { ...state, fetching: true, error: false }
    case GET_SERVICES_SUCCESS:
      return { ...state, fetching: false, services: action.payload.services, error: false }
    case GET_SERVICES_ERROR:
      return { ...state, fetching: false, error: action.payload.error }

    case GET_USER_SERVICES:
      return { ...state, fetching: true, error: false }
    case GET_USER_SERVICES_SUCCESS:
      return { ...state, fetching: false, userServices: action.payload.userServices, error: false }
    case GET_USER_SERVICES_ERROR:
      return { ...state, fetching: false, error: action.payload.error }

    case SAVE_USER_SERVICES:
      return { ...state, fetching: true, error: false }
    case SAVE_USER_SERVICES_SUCCESS:
      return { ...state, fetching: false, userServices: [...action.payload.userServices], error: false }
    case SAVE_USER_SERVICES_ERROR:
      return { ...state, fetching: false, error: action.payload.error }

    case DELETE_USER_SERVICES:
      return { ...state, fetching: true, error: false }
    case DELETE_USER_SERVICES_SUCCESS:
      return { ...state, fetching: false, userServices: [...action.payload.newState], error: false }
    case DELETE_USER_SERVICES_ERROR:
      return { ...state, fetching: false, error: action.payload.error }

    case RELOAD_SERVICE:
      return { ...state, fetching: true, error: false }
    case RELOAD_SERVICE_SUCCESS:
      return { ...state, fetching: false, error: false }
    case RELOAD_SERVICE_ERROR:
      return { ...state, fetching: false, error: action.payload.error }

    default:
      return state
  }
}

// actions
export const getServicesAction = () => async (dispatch, getState) => {
  let error
  const { user: { token } } = getState()

  dispatch({ type: GET_SERVICES })

  try {
    const data = await fetch(process.env.REACT_APP_API_URL + '/services', {
      method: 'GET',
      headers: {
        'content-Type': 'application/json',
        Authorization: 'bearer ' + token
      }
    })

    const body = await data.json()

    if (data.status === 200) {
      dispatch({
        type: GET_SERVICES_SUCCESS,
        payload: { services: body }
      })
      return
    }
    if (data.status === 401) {
      dispatch(logoutAction())
      return
    }

    error = 'Ups! Something went wrong. Please, try again'
  } catch (er) {
    error = er
  }

  dispatch({
    type: GET_SERVICES_ERROR,
    payload: { error }
  })

  window.M.toast({ html: error })
}
export const getUserServicesAction = () => async (dispatch, getState) => {
  let error
  const { user: { token } } = getState()

  dispatch({ type: GET_USER_SERVICES })

  try {
    const data = await fetch(process.env.REACT_APP_API_URL + '/services/me', {
      method: 'GET',
      headers: {
        'content-Type': 'application/json',
        Authorization: 'bearer ' + token
      }
    })

    const body = await data.json()

    if (data.status === 200) {
      dispatch({
        type: GET_USER_SERVICES_SUCCESS,
        payload: { userServices: body }
      })
      return
    }
    if (data.status === 401) {
      dispatch(logoutAction())
      return
    }

    error = 'Ups! Something went wrong. Please, try again'
  } catch (er) {
    error = er
  }

  dispatch({
    type: GET_USER_SERVICES_ERROR,
    payload: { error }
  })

  window.M.toast({ html: error })
}
export const saveUserServiceAction = (data, mode = 'CREATE') => async (dispatch, getState) => {
  let error
  let method = 'POST'
  const { user: { token } } = getState()

  dispatch({ type: SAVE_USER_SERVICES })

  if (mode === 'UPDATE') {
    method = 'PATCH'
  }

  try {
    const response = await fetch(process.env.REACT_APP_API_URL + '/services/me', {
      method,
      headers: {
        'content-Type': 'application/json',
        Authorization: 'bearer ' + token
      },
      body: JSON.stringify(data)
    })

    const body = await response.json()

    if (response.status === 200) {
      let { services: { userServices } } = getState()
      if (mode === 'CREATE') {
        userServices = [...userServices, body]
      } else if (mode === 'UPDATE') {
        userServices = userServices.map(userService => userService.id === body.id ? body : userService)
      }
      dispatch({
        type: SAVE_USER_SERVICES_SUCCESS,
        payload: { userServices }
      })
      return true
    }
    if (response.status === 401) {
      dispatch(logoutAction())
      return false
    }

    error = JSON.stringify(body.errors[0]).replace(/{|}|"/g, ' ').trim()
  } catch (er) {
    error = er
  }

  dispatch({
    type: SAVE_USER_SERVICES_ERROR,
    payload: { error }
  })

  window.M.toast({ html: error })
  return false
}
export const deleteUserServiceAction = userServiceId => async (dispatch, getState) => {
  let error
  const { user: { token } } = getState()

  dispatch({ type: DELETE_USER_SERVICES })

  try {
    const response = await fetch(process.env.REACT_APP_API_URL + '/services/me', {
      method: 'DELETE',
      headers: {
        'content-Type': 'application/json',
        Authorization: 'bearer ' + token
      },
      body: JSON.stringify({ id: userServiceId })
    })

    if (response.status === 200) {
      const { services: { userServices } } = getState()
      const newState = userServices.filter(userService => userService.id !== userServiceId)

      dispatch({
        type: DELETE_USER_SERVICES_SUCCESS,
        payload: { newState }
      })

      return true
    } else if (response.status === 401) {
      dispatch(logoutAction())
    }
  } catch (er) {
    console.log(er)
    error = er
  }

  dispatch({
    type: DELETE_USER_SERVICES_ERROR,
    payload: { error }
  })

  window.M.toast({ html: error })
  return false
}
export const reloadServiceAction = (userServiceId, from) => async (dispatch, getState) => {
  let error
  const { user: { token } } = getState()
  const body = { userServiceId }
  if (from) {
    body.from = from
  }

  dispatch({ type: RELOAD_SERVICE })

  try {
    const response = await fetch(process.env.REACT_APP_API_URL + '/bills/refresh', {
      method: 'POST',
      headers: {
        'content-Type': 'application/json',
        Authorization: 'bearer ' + token
      },
      body: JSON.stringify(body)
    })

    if (response.status === 200) {
      dispatch({
        type: RELOAD_SERVICE_SUCCESS
      })

      return true
    } else if (response.status === 401) {
      dispatch(logoutAction())
    }
  } catch (er) {
    console.log(er)
    error = er
  }

  dispatch({
    type: RELOAD_SERVICE_ERROR,
    payload: { error }
  })

  window.M.toast({ html: error })
  return false
}
