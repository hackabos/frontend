const initialSate = {
  fetching: false,
  token: null,
  profile: null
}

const SIGNUP = 'SIGNUP'
const SIGNUP_SUCCESS = 'SIGNUP_SUCCESS'
const SIGNUP_ERROR = 'SIGNUP_ERROR'

const LOGIN = 'LOGIN'
const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
const LOGIN_ERROR = 'LOGIN_ERROR'

const RECOVER_PASS = 'RECOVER_PASS'
const RECOVER_PASS_SUCCESS = 'RECOVER_PASS_SUCCESS'
const RECOVER_PASS_ERROR = 'RECOVER_PASS_ERROR'

const LOGOUT = 'LOGOUT'

// reducer
export default function reducer (state = initialSate, action) {
  switch (action.type) {
    case SIGNUP:
      return { ...state, fetching: true }
    case SIGNUP_SUCCESS:
      return { ...state, fetching: false, error: null }
    case SIGNUP_ERROR:
      return { ...state, fetching: false, error: action.payload }

    case LOGIN:
      return { ...state, fetching: true }
    case LOGIN_SUCCESS:
      return { ...state, fetching: false, token: action.payload.token, profile: action.payload.profile, error: null }
    case LOGIN_ERROR:
      return { ...state, fetching: false, error: action.payload }

    case RECOVER_PASS:
      return { ...state, fetching: true }
    case RECOVER_PASS_SUCCESS:
      return { ...state, fetching: false, error: null }
    case RECOVER_PASS_ERROR:
      return { ...state, fetching: false, error: action.payload }

    case LOGOUT:
      return { ...state, token: null, profile: null }
    default:
      return state
  }
}

// actions
export const signupAction = signupData => async (dispatch, getState) => {
  let error

  dispatch({ type: SIGNUP })

  try {
    const data = await fetch(process.env.REACT_APP_API_URL + '/users/signup', {
      method: 'POST',
      headers: {
        'content-Type': 'application/json'
      },
      body: JSON.stringify(signupData)
    })

    const body = await data.json()

    if (data.status === 200) {
      dispatch({
        type: SIGNUP_SUCCESS,
        payload: body
      })
      window.M.toast({ html: 'Your account was succesfully created. Check your email account to activate' })
      return true
    }

    error = JSON.stringify(body.errors[0]).replace(/{|}|"/g, ' ').trim()
  } catch (er) {
    error = er
  }

  dispatch({
    type: SIGNUP_ERROR,
    payload: error
  })

  window.M.toast({ html: error })
  return false
}

export const loginAction = loginData => async (dispatch, getState) => {
  let error

  dispatch({ type: LOGIN })

  try {
    const data = await fetch(process.env.REACT_APP_API_URL + '/users/login', {
      method: 'POST',
      headers: {
        'content-Type': 'application/json'
      },
      body: JSON.stringify(loginData)
    })

    const body = await data.json()

    if (data.status === 200) {
      dispatch({
        type: LOGIN_SUCCESS,
        payload: body
      })
      localStorage.setItem('token', body.token)
      return
    }

    error = JSON.stringify(body.errors[0]).replace(/{|}|"/g, ' ').trim()
  } catch (er) {
    error = er
  }

  dispatch({
    type: LOGIN_ERROR,
    payload: error
  })

  window.M.toast({ html: error })
}

export const passRecoveryAction = username => async (dispatch, getState) => {
  let error

  dispatch({ type: RECOVER_PASS })

  try {
    const data = await fetch(process.env.REACT_APP_API_URL + '/users/recover-pass', {
      method: 'POST',
      headers: {
        'content-Type': 'application/json'
      },
      body: JSON.stringify(username)
    })

    const body = await data.json()

    if (data.status === 200) {
      dispatch({
        type: RECOVER_PASS_SUCCESS,
        payload: body
      })
      window.M.toast({ html: 'You have an email with your new password!' })
      return true
    }

    error = JSON.stringify(body.errors[0]).replace(/{|}|"/g, ' ').trim()
  } catch (er) {
    error = er
  }

  dispatch({
    type: RECOVER_PASS_ERROR,
    payload: error
  })

  window.M.toast({ html: error })
  return false
}

export const restoreSession = () => async dispatch => {
  let error
  const token = localStorage.getItem('token')

  if (!token) return

  dispatch({ type: LOGIN })

  try {
    const data = await fetch(process.env.REACT_APP_API_URL + '/users/me', {
      method: 'GET',
      headers: {
        'content-Type': 'application/json',
        Authorization: 'bearer ' + token
      }
    })

    const body = await data.json()

    if (data.status === 200) {
      dispatch({
        type: LOGIN_SUCCESS,
        payload: { token, profile: body }
      })

      return
    }

    error = body
  } catch (er) {
    console.log(er)
    error = er
  }

  dispatch({
    type: LOGIN_ERROR,
    payload: error
  })
}

export const logoutAction = () => dispatch => {
  localStorage.removeItem('token')
  dispatch({ type: LOGOUT })
}
