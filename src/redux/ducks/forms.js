import { logoutAction } from './user'

const initialSate = {
  fetching: false,
  formType: null,
  error: null
}

const SEND_FORM = 'SEND_FORM'
const SEND_FORM_SUCCESS = 'SEND_FORM_SUCCESS'
const SEND_FORM_ERROR = 'SEND_FORM_ERROR'

// reducer
export default function reducer (state = initialSate, action) {
  switch (action.type) {
    case SEND_FORM:
      return { fetching: true, formType: action.payload.formType, error: null }
    case SEND_FORM_SUCCESS:
      return { fetching: false, formType: null, error: null }
    case SEND_FORM_ERROR:
      return { fetching: false, formType: null, error: action.payload.error }
    default:
      return state
  }
}

// actions
export const sendForm = (formType, form) => async (dispatch, getState) => {
  let error
  const { user: { token } } = getState()

  dispatch({
    type: SEND_FORM,
    payload: { formType }
  })

  try {
    const data = await fetch(process.env.REACT_APP_API_URL + '/users/me', {
      method: 'PATCH',
      headers: {
        'content-Type': 'application/json',
        Authorization: 'bearer ' + token
      },
      body: JSON.stringify(form)
    })

    const body = await data.json()

    if (data.status === 200) {
      dispatch({ type: SEND_FORM_SUCCESS })
      window.M.toast({ html: 'Your data was saved!' })
      return
    }
    if (data.status === 401) {
      dispatch(logoutAction())
      return
    }

    error = JSON.stringify(body.errors[0]).replace(/{|}|"/g, ' ').trim()
  } catch (err) {
    console.log(err)
    error = 'Ups! Something went wrong. Please, try again'
  }

  dispatch({
    type: SEND_FORM_ERROR,
    payload: { error }
  })

  window.M.toast({ html: error })
}
