const initialSate = {
  showing: false,
  modalType: null,
  data: null
}

// Modal type constants
export const LOGIN_TYPE = 'LOGIN'
export const RELOAD_SERVICE_TYPE = 'RELOAD_SERVICE_TYPE'

const SHOW_MODAL = 'SHOW_MODAL'
const HIDE_MODAL = 'HIDE_MODAL'

// reducer
export default function reducer (state = initialSate, action) {
  switch (action.type) {
    case SHOW_MODAL:
      return { showing: true, modalType: action.modalType, data: action.data }
    case HIDE_MODAL:
      return { showing: false, modalType: null, data: null }
    default:
      return state
  }
}

// actions
export const showModal = (modalType, data) => dispatch => {
  dispatch({
    type: SHOW_MODAL,
    modalType,
    data
  })
}

export const hideModal = () => dispatch => {
  dispatch({
    type: HIDE_MODAL
  })
}
