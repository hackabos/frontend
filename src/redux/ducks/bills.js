import { logoutAction } from './user'

const initialSate = {
  fetching: false,
  billsData: null
}

const GET_BILLS = 'GET_BILLS'
const GET_BILLS_SUCCESS = 'GET_BILLS_SUCCESS'
const GET_BILLS_ERROR = 'GET_BILLS_ERROR'
const DOWNLOAD_BILL = 'DOWNLOAD_BILL'
const DOWNLOAD_BILL_SUCCESS = 'DOWNLOAD_BILL_SUCCESS'
const DOWNLOAD_BILL_ERROR = 'DOWNLOAD_BILL_ERROR'

// reducer
export default function reducer (state = initialSate, action) {
  switch (action.type) {
    case GET_BILLS:
      return { ...state, fetching: true, error: false }
    case GET_BILLS_SUCCESS:
      return { ...state, fetching: false, billsData: action.payload.bills, error: false }
    case GET_BILLS_ERROR:
      return { ...state, fetching: false, error: action.payload.error }
    case DOWNLOAD_BILL:
      return { ...state, fetching: true, error: false }
    case DOWNLOAD_BILL_SUCCESS:
      return { ...state, fetching: false, error: false }
    case DOWNLOAD_BILL_ERROR:
      return { ...state, fetching: false, error: action.payload.error }
    default:
      return state
  }
}

// actions
export const getBillsAction = () => async (dispatch, getState) => {
  let error
  const { user: { token } } = getState()

  dispatch({ type: GET_BILLS })

  try {
    const data = await fetch(process.env.REACT_APP_API_URL + '/bills', {
      method: 'GET',
      headers: {
        'content-Type': 'application/json',
        Authorization: 'bearer ' + token
      }
    })

    const body = await data.json()

    if (data.status === 200) {
      dispatch({
        type: GET_BILLS_SUCCESS,
        payload: { bills: body }
      })
      return
    }
    if (data.status === 401) {
      dispatch(logoutAction())
      return
    }

    error = JSON.stringify(body.errors[0]).replace(/{|}|"/g, ' ').trim()
  } catch (er) {
    error = er
  }

  dispatch({
    type: GET_BILLS_ERROR,
    payload: { error }
  })

  window.M.toast({ html: error })
}

export const download = (userServiceId, number) => async (dispatch, getState) => {
  let error
  const { user: { token } } = getState()

  dispatch({ type: DOWNLOAD_BILL })

  try {
    const data = await fetch(process.env.REACT_APP_API_URL + '/bills/download', {
      method: 'POST',
      headers: {
        'content-Type': 'application/json',
        Authorization: 'bearer ' + token
      },
      body: JSON.stringify({ userServiceId, number })
    })

    const body = await data.blob()

    if (data.status === 200) {
      const file = new Blob([body], {
        type: 'application/pdf'
      })
      const fileURL = URL.createObjectURL(file)
      window.open(fileURL, '_blank')

      dispatch({
        type: DOWNLOAD_BILL_SUCCESS
      })
      return
    }
    if (data.status === 401) {
      dispatch(logoutAction())
      return
    }

    error = data.statusText
  } catch (er) {
    console.log(er)
    error = 'Ups! Something went wrong. Please, try again'
  }

  dispatch({
    type: DOWNLOAD_BILL_ERROR,
    payload: error
  })

  window.M.toast({ html: error })
}
