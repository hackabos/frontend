import { createStore, combineReducers, compose, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import userReducer from './ducks/user'
import modalsReducer from './ducks/modals'
import formsReducer from './ducks/forms'
import billsReducer from './ducks/bills'
import servicesReducer from './ducks/services'
import notificationsReducer from './ducks/notifications'

const rootReducer = combineReducers({
  user: userReducer,
  modals: modalsReducer,
  forms: formsReducer,
  bills: billsReducer,
  services: servicesReducer,
  notifications: notificationsReducer
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

export default function generateStore () {
  const store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(thunk))
  )

  return store
}
