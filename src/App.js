import React, { useEffect } from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { restoreSession } from './redux/ducks/user'
import Dashboard from './react-components/Dashboard'
import Public from './react-components/Public'
import './scss/main.scss'

const App = () => {
  const dispatch = useDispatch()
  const { token } = useSelector(s => s.user)

  useEffect(() => {
    dispatch(restoreSession())
  }, [dispatch])
  return (
    <Router>
      {token ? <Dashboard /> : <Public />}
    </Router>
  )
}

export default App
